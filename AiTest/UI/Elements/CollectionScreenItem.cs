﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AiTest.UI.Elements
{
    public class CollectionScreenItem
    {
        public Texture2D Texture { get; set; }
        public Point Position { get; set; }

        private readonly Rectangle _rectangle;

        public CollectionScreenItem(Texture2D texture, Point position)
        {
            Texture = texture;
            Position = position;

            _rectangle = new Rectangle(position, new Point(texture.Width, texture.Height));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, _rectangle, Microsoft.Xna.Framework.Color.White);
        }
    }
}