﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AiTest.UI.Elements
{
    public class CollectionScreen
    {
        private Button _buttonLeft;
        private Button _buttonRight;

        public Texture2D Texture { get; set; }
        public Point Position { get; set; }
        public List<CollectionScreenItem> IconObjects { get; set; }

        private List<CollectionScreenItem> _visibleIconObjects;
        private int _page;
        private readonly Rectangle _rectangle;
        private readonly int _itemWidth;
        private readonly int _amountOfItemsInBar = 5;

        /// <summary>
        /// A collection bar of items to drag onto the grid
        /// </summary>
        /// <param name="viewPort">The viewport of the game to find the width and height of the device</param>
        /// <param name="backTexture">The texture in the background of the screen</param>
        /// <param name="buttonLeftDefaultTexture">The left default state button texture</param>
        /// <param name="buttonLeftOnTappedTexture">The left on tapped state button texture</param>
        /// <param name="buttonRightDefaultTexture">The right default state button texture</param>
        /// <param name="buttonRightOnTappedTexture">The right on tapped state button texture</param>
        /// <param name="position">The position of the collection bar</param>
        public CollectionScreen(Viewport viewPort, 
                                Texture2D backTexture, 
                                Texture2D buttonLeftDefaultTexture,
                                Texture2D buttonLeftOnTappedTexture,
                                Texture2D buttonRightDefaultTexture,
                                Texture2D buttonRightOnTappedTexture,
                                Point position)
        {
            Texture = backTexture;
            Position = position;
            IconObjects = new List<CollectionScreenItem>();
            _visibleIconObjects = new List<CollectionScreenItem>();

            _rectangle = new Rectangle(position, new Point(viewPort.Width, backTexture.Height));

            // TODO Make background a texture that repeats itself (like a tile)

            // Initialize buttons
            _buttonLeft = new Button(buttonLeftDefaultTexture, 
                                     buttonLeftOnTappedTexture, 
                                     new Point(_rectangle.X, (backTexture.Height / 2) - (buttonLeftDefaultTexture.Height / 2)));
            _buttonLeft.Pressed += Left_Button_Pressed;

            _buttonRight = new Button(buttonRightDefaultTexture,
                                      buttonRightOnTappedTexture,
                                      new Point((_rectangle.X + backTexture.Width) - buttonRightDefaultTexture.Width, (backTexture.Height / 2) - (buttonLeftDefaultTexture.Height / 2)));
            _buttonRight.Pressed += Right_Button_Pressed;

            // TODO Make amount of elements dependent on the viewport
            _itemWidth = (viewPort.Width - (buttonLeftDefaultTexture.Width + buttonRightDefaultTexture.Width)) / _amountOfItemsInBar;
        }

        /// <summary>
        /// Add an item to the icon list
        /// </summary>
        /// <param name="itemTexture">The texture of the icon</param>
        public void AddItem(Texture2D itemTexture)
        {
            CollectionScreenItem newItem = new CollectionScreenItem(itemTexture, GetElementPosition(itemTexture, IconObjects.Count));
            IconObjects.Add(newItem);
            RebuildVisibleIconObjects();
        }

        public void Update()
        {
            _buttonLeft.Update();
            _buttonRight.Update();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, _rectangle, Color.White);

            foreach (CollectionScreenItem item in _visibleIconObjects)
            {
                item.Draw(spriteBatch);
            }

            _buttonLeft.Draw(spriteBatch);
            _buttonRight.Draw(spriteBatch);
        }

        private void Left_Button_Pressed(object sender, EventArgs e)
        {
            // Cannot go lower than zero
            if (_page > 0)
            {
                _page--;
                RebuildVisibleIconObjects();
            }
        }

        private void Right_Button_Pressed(object sender, EventArgs e)
        {
            // TODO Make amount of elements dependent on the viewport
            // Cannot go higher than the limit of list
            if ((_amountOfItemsInBar + _page) < IconObjects.Count)
            {
                _page++;
                RebuildVisibleIconObjects();
            }
        }

        /// <summary>
        /// Rebuilds the visible icon objects list based on the current page
        /// </summary>
        private void RebuildVisibleIconObjects()
        {
            _visibleIconObjects = new List<CollectionScreenItem>();

            // TODO Make amount of elements dependent on the viewport
            int limit = _page + _amountOfItemsInBar;
            for (int i=_page; i < limit; i++)
            {
                if (i <= (IconObjects.Count - 1))
                {
                    _visibleIconObjects.Add(new CollectionScreenItem(IconObjects[i].Texture, GetElementPosition(IconObjects[i].Texture, (i - _page))));
                }
            }
        }

        /// <summary>
        /// Gets where to draw the icon based on texture and position number
        /// </summary>
        /// <param name="texture">The texture of the icon to draw</param>
        /// <param name="position">The position number of the item within the collection screen</param>
        /// <returns>A Point object of where to draw the icon</returns>
        private Point GetElementPosition(Texture2D texture, int position)
        {
            int xPos = (int)((_itemWidth * 0.5) - (texture.Width * 0.5)) + (position * _itemWidth) + _buttonLeft.DefaultTexture.Width + _rectangle.X;
            int yPos = (Texture.Height / 2) - (texture.Height / 2);

            return new Point(xPos, yPos);
        }
    }
}