﻿using System;
using AiTest.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AiTest.UI
{
    public class Button
    {
        public Texture2D DefaultTexture { get; set; }
        public Texture2D OnPressedTexture { get; set; }
        public Point Position { get; set; }

        private Rectangle _defaultRectangle;
        private Rectangle _onPressedRectangle;
        private bool _pressed = false;

        public Button(Texture2D defaultTexture, Texture2D onPressedTexture, Point position)
        {
            DefaultTexture = defaultTexture;
            OnPressedTexture = onPressedTexture;
            Position = position;

            _defaultRectangle = new Rectangle(position, new Point(defaultTexture.Width, defaultTexture.Height));
            _onPressedRectangle = new Rectangle(position, new Point(onPressedTexture.Width, onPressedTexture.Height));
        }

        public event EventHandler Pressed;

        public void Update()
        {
            if (TouchStateManager.PressedOrHolding(_defaultRectangle))
            {
                _pressed = true;
            }
            else if(_pressed)
            {
                _pressed = false;
                if (TouchStateManager.Released(_defaultRectangle))
                {
                    Pressed?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (_pressed)
            {
                spriteBatch.Draw(OnPressedTexture, _onPressedRectangle, Color.White);
            }
            else
            {
                spriteBatch.Draw(DefaultTexture, _defaultRectangle, Color.White);
            }
        }
    }
}