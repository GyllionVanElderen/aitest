﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Objects;

namespace AiTest.Objects
{
    public class TestObject : SolidGridObject, IDetectable
    {
        /// <summary>
        /// A test object to test a detectable object
        /// </summary>
        /// <param name="texture">The texture of the object</param>
        /// <param name="gridPosition">Which X and Y position on the grid the object will get</param>
        /// <param name="width">How much grid squares the object takes horizontally</param>
        /// <param name="height">How much grid squares the object takes vertically</param>
        public TestObject(Texture2D texture, Vector2 gridPosition, int width, int height, Vector2 gridOffset, float columnWidthHeight) : base(texture, gridPosition, gridOffset, columnWidthHeight)
        {
            GridPosition = gridPosition;
        }
    }
}