﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Field;
using MonoGrid.Helpers;
using MonoGrid.Objects;

namespace AiTest.Objects
{
    public class Character : MoveableGridObject
    {
        public Character(GraphicsDeviceManager graphics, Vector2 gridPosition, Grid grid) : base(grid.ColumnWidthHeight)
        {
            GridPosition = gridPosition;

            // Settings texture
            Texture = new Texture2D(graphics.GraphicsDevice, (int)grid.ColumnWidthHeight, (int)grid.ColumnWidthHeight);

            Color[] data = new Color[(int)grid.ColumnWidthHeight * (int)grid.ColumnWidthHeight];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = Color.DarkRed;
            }
            Texture.SetData(data);

            // Choose random direction to walk to
            Array values = Enum.GetValues(typeof(MonoGrid.Enums.Direction));
            Random random = new Random();
            MonoGrid.Enums.Direction randomDirection = (MonoGrid.Enums.Direction)values.GetValue(random.Next(values.Length));

            SelectDirection(randomDirection, grid);
        }

        public override void Update(GameTime gameTime, float fps, Grid grid)
        {
            base.Update(gameTime, fps, grid);
        }

        public override void BeforeEnteringNewGridSquare(Vector2 gridSquare, Grid grid)
        {
            IGridObject gridObjectToCheck = grid.GetGridObjectObject(gridSquare);
            if (gridObjectToCheck is IDetectable)
            {
                MonoGrid.Enums.Direction oppositeDirection = DirectionHelper.GetOppositeDirection(DirectionQueue.First());
                DirectionQueue.Clear();
                FillDirectionQueue(oppositeDirection, grid);
            }
        }

        private void SelectDirection(MonoGrid.Enums.Direction direction, Grid grid)
        {
            FillDirectionQueue(direction, grid);
        }

        private void FillDirectionQueue(MonoGrid.Enums.Direction direction, Grid grid)
        {
            int times = (int) DirectionHelper.GetLeftOverDistance(direction, grid.Width, grid.Height, GridPosition);

            for (int i=0; i<times; i++)
            {
                Move(direction);
            }
        }
    }
}