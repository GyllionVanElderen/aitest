using System;
using System.Collections.Generic;
using AiTest.Input;
using AiTest.Objects;
using AiTest.UI;
using AiTest.UI.Elements;
using Android.OS;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGrid.Field;
using MonoGrid.Helpers;
using MonoGrid.Objects;

namespace AiTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private SpriteFont spriteFont;

        private FrameRateCounter frCounter;
        private FpsHelper fpsHelperUpdate;
        private FpsHelper fpsHelperDraw;
        private Grid grid;

        private CollectionScreen collectionScreen;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;
            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            spriteFont = Content.Load<SpriteFont>("Default");

            // UI
            collectionScreen = new CollectionScreen(graphics.GraphicsDevice.Viewport, 
                                                    Content.Load<Texture2D>("collection_screen_backgr"), 
                                                    Content.Load<Texture2D>("arrow_left"), 
                                                    Content.Load<Texture2D>("arrow_left_onPressed"), 
                                                    Content.Load<Texture2D>("arrow_right"), 
                                                    Content.Load<Texture2D>("arrow_right_onPressed"), 
                                                    new Point(0, 0));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_1"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_1"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_1"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_2"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_2"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_2"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_2"));
            collectionScreen.AddItem(Content.Load<Texture2D>("Log_2"));
   

            // Randomly generate a direction
            Array values = Enum.GetValues(typeof(MonoGrid.Enums.Direction));
            Random random = new Random();
            MonoGrid.Enums.Direction randomDirection = (MonoGrid.Enums.Direction)values.GetValue(random.Next(values.Length));

            grid = new Grid(GraphicsDevice, spriteFont, 15, 20, 50, new Vector2(100, 400));
            grid.Add(new AiTest.Objects.TestObject(Content.Load<Texture2D>("Log"), new Vector2(random.Next(9), random.Next(10)), 2, 1, grid.Offset, grid.ColumnWidthHeight));
            grid.Add(new Character(graphics, new Vector2(5, 5), grid));

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            frCounter = new FrameRateCounter(Content);
            fpsHelperUpdate = new FpsHelper();
            fpsHelperDraw = new FpsHelper();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            TouchStateManager.Update();

            grid.Update(gameTime, fpsHelperUpdate.GetFps((float)gameTime.TotalGameTime.TotalSeconds));
            frCounter.Update(gameTime);
            collectionScreen.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            grid.Draw(gameTime, fpsHelperDraw.GetFps((float)gameTime.TotalGameTime.TotalSeconds), spriteBatch);
            frCounter.Draw(spriteBatch);
            collectionScreen.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
