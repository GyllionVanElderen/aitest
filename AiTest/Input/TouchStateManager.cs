﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace AiTest.Input
{
    public static class TouchStateManager
    {
        private static TouchCollection _touchState;

        public static void Update()
        {
            _touchState = TouchPanel.GetState();
        }

        public static bool Pressed(Rectangle rectangle)
        {
            return _touchState.Any(x => rectangle.Contains(x.Position) && x.State == TouchLocationState.Pressed);
        }

        public static bool Holding(Rectangle rectangle)
        {
            return _touchState.Any(x => rectangle.Contains(x.Position) && x.State == TouchLocationState.Moved);
        }

        public static bool PressedOrHolding(Rectangle rectangle)
        {
            return _touchState.Any(x => rectangle.Contains(x.Position) && (x.State == TouchLocationState.Pressed || x.State == TouchLocationState.Moved));
        }

        public static bool Released(Rectangle rectangle)
        {
            return _touchState.Any(x => rectangle.Contains(x.Position) && x.State == TouchLocationState.Released);
        }
    }
}