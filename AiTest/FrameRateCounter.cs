﻿using System.Globalization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Helpers;

namespace AiTest
{
    public class FrameRateCounter
    {
        private readonly SpriteFont _spriteFont;
        private readonly FpsHelper _fpsHelperUpdate;
        private readonly FpsHelper _fpsHelperDraw;

        private double _seconds;
        private float _updateFps;
        private float _drawFps;

        public FrameRateCounter(ContentManager content)
        {
            _spriteFont = content.Load<SpriteFont>("Default");
            _fpsHelperUpdate = new FpsHelper();
            _fpsHelperDraw = new FpsHelper();
        }

        public void Update(GameTime gameTime)
        {
            _seconds = gameTime.TotalGameTime.TotalSeconds;
            _updateFps = _fpsHelperUpdate.GetFps(_seconds);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            _drawFps = _fpsHelperDraw.GetFps(_seconds);
            spriteBatch.DrawString(_spriteFont, "Update: " + _updateFps.ToString(CultureInfo.InvariantCulture), new Vector2(50, 50), Color.DarkRed);
            spriteBatch.DrawString(_spriteFont, "Draw: " + _drawFps.ToString(CultureInfo.InvariantCulture), new Vector2(50, 100), Color.DarkRed);
        }
    }
}