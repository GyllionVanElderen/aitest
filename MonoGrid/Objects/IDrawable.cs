﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGrid.Objects
{
    public interface IDrawable : IGridObject
    {
        void Draw(GameTime gameTime, float fps, SpriteBatch spriteBatch, Vector2 gridOffset, float columnWidthHeight, SpriteFont spriteFont);
    }
}