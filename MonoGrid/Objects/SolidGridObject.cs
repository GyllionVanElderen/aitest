﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Helpers;

namespace MonoGrid.Objects
{
    public class SolidGridObject : IDrawable
    {
        public Vector2 GridPosition { get; set; }
        public Vector2 RenderPosition { get; set; }
        public Texture2D Texture { get; set; }

        public SolidGridObject(Texture2D texture, Vector2 gridPosition, Vector2 gridOffset, float columnWidthHeight)
        {
            Texture = texture;
            Vector2 renderPositionNotCentered =RenderHelper.GetRenderPosition(gridPosition, gridOffset, columnWidthHeight);
            RenderPosition = RenderHelper.GetRenderPositionCentered(new Vector2(Texture.Width, Texture.Height), renderPositionNotCentered, columnWidthHeight);
        }

        public void Draw(GameTime gameTime, float fps, SpriteBatch spriteBatch, Vector2 gridOffset, float columnWidthHeight, SpriteFont spriteFont)
        {
            // For debug purposes
            spriteBatch.DrawString(spriteFont, GridPosition.X + " : " + GridPosition.Y, new Vector2(RenderPosition.X - 10, RenderPosition.Y - 30), Color.LightGoldenrodYellow);

            spriteBatch.Draw(Texture, RenderPosition, Color.White);
        }
    }
}