﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGrid.Objects
{
    public interface IGridObject
    {
        /// <summary>
        /// The current position in grid spaces (which column, which row?)
        /// </summary>
        Vector2 GridPosition { get; set; }

        /// <summary>
        /// The position where to render the grid object
        /// </summary>
        Vector2 RenderPosition { get; set; }

        /// <summary>
        /// The texture of the grid object
        /// </summary>
        Texture2D Texture { get; set; }

    }
}