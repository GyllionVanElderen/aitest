﻿using System.Collections.Generic;
using System.Linq;
using MonoGrid.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Enums;
using MonoGrid.Field;

namespace MonoGrid.Objects
{
    public class MoveableGridObject : IUpdateable, IDrawable
    {
        public Vector2 GridPosition { get; set; }
        public Vector2 RenderPosition { get; set; }
        public Texture2D Texture { get; set; }
        public List<Direction> DirectionQueue { get; private set; }
        public double AnimationSpeedInSeconds{ get; set; }

        private float _animationPosition;
        private float _pixelsPerLoop;
        private bool _animationPlaying = false;
        private double _timeLeftInSeconds;
        private double _timeStamp;
        private float _columnWidtHeight;

        /// <summary>
        /// How much pixels the object is in the animation of moving (within a gridsquare)
        /// </summary>
        protected Vector2 AnimationPixelPosition { get; set; }

        protected MoveableGridObject(float columnWidthHeight)
        {
            AnimationPixelPosition = new Vector2(0, 0);
            DirectionQueue = new List<Direction>();

            _columnWidtHeight = columnWidthHeight;
            _animationPosition = _columnWidtHeight;

            // TODO Make Animation speed dynamic
            AnimationSpeedInSeconds = 1;
        }

        /// <summary>
        /// Hook that fires when a new grid position has been entered
        /// </summary>
        /// <param name="newGridPosition">The new grid position retrieved entered</param>
        /// <param name="grid">The parent grid</param>
        public virtual void OnEnterNewGridSquare(Vector2 newGridPosition, Grid grid)
        {
        }

        /// <summary>
        /// Hook that fires before a new grid position has been entered
        /// </summary>
        /// <param name="toGridPosition">The grid position that is about to be entered</param>
        /// <param name="grid">The parent grid</param>
        public virtual void BeforeEnteringNewGridSquare(Vector2 toGridPosition, Grid grid)
        {
        }

        /// <summary>
        /// Add a direction to the direction queue
        /// </summary>
        /// <param name="direction">The direction to add</param>
        public void Move(Direction direction)
        {
            DirectionQueue.Add(direction);
        }

        public virtual void Update(GameTime gameTime, float fps, Grid grid)
        {
            // Step 1: Is there something in the DirectionQueue?
            if (DirectionQueue.Count > 0 && !_animationPlaying && gameTime.TotalGameTime.Seconds > 0)
            {
                BeforeEnteringNewGridSquare(DirectionHelper.GetTargetPosition(GridPosition, DirectionQueue.First()), grid);
                StartAnimation(gameTime);

                _pixelsPerLoop = DirectionHelper.ToPixelsPerLoop((_columnWidtHeight - _animationPosition), (float)_timeLeftInSeconds, fps);
            }

            // Step 2: Animation position < gridWidthHeight?
            if (_animationPlaying)
            {
                if (_animationPosition < _columnWidtHeight && _pixelsPerLoop > 0)
                {
                    // Step 3: Play animation frame
                    _timeLeftInSeconds = GetTimeLeftInSeconds(gameTime);

                    RenderPosition = DirectionHelper.GetRenderPosition(GridPosition,
                                                                       DirectionHelper.GetTargetPosition(GridPosition, DirectionQueue.First()),
                                                                       _columnWidtHeight,
                                                                       grid.Offset,
                                                                       _animationPosition);
                    _animationPosition += _pixelsPerLoop;
                }
                else
                {
                    StopAnimation(grid);
                }
            }
        }

        public void Draw(GameTime gameTime, float fps, SpriteBatch spriteBatch, Vector2 gridOffset, float columnWidthHeight, SpriteFont spriteFont)
        {
            // For debug purposes
            spriteBatch.DrawString(spriteFont, GridPosition.X + " : " + GridPosition.Y, new Vector2(RenderPosition.X - 10, RenderPosition.Y - 30), Color.LightGoldenrodYellow);

            spriteBatch.Draw(Texture, RenderPosition, Color.White);
        }

        /// <summary>
        /// Retireves how much time there is left to finish the current animation
        /// Time will be retrieved in seconds
        /// </summary>
        /// <param name="gameTime">The GameTime object retrieved from the Update method</param>
        /// <returns>The amount of time left for the animation in seconds</returns>
        private double  GetTimeLeftInSeconds(GameTime gameTime)
        {
            return (_timeStamp + AnimationSpeedInSeconds) - gameTime.TotalGameTime.TotalSeconds;
        }

        /// <summary>
        /// Start the animation (First Direction in the queue)
        /// </summary>
        /// <param name="gameTime">The GameTime object retrieved from the Update method</param>
        private void StartAnimation(GameTime gameTime)
        {
            _animationPlaying = true;
            _timeStamp = gameTime.TotalGameTime.TotalSeconds;
            _timeLeftInSeconds = GetTimeLeftInSeconds(gameTime);
            _animationPosition = 0;
        }

        /// <summary>
        /// Stop current the animation
        /// </summary>
        /// <param name="grid">The parent grid given by the Update method</param>
        private void StopAnimation(Grid grid)
        {
            GridPosition = DirectionHelper.GetTargetPosition(GridPosition, DirectionQueue.First());
            DirectionQueue.RemoveAt(0);
            OnEnterNewGridSquare(GridPosition, grid);

            _animationPlaying = false;
            _timeLeftInSeconds = 0;
        }
    }
}