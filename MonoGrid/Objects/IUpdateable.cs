﻿using Microsoft.Xna.Framework;
using MonoGrid.Field;

namespace MonoGrid.Objects
{
    public interface IUpdateable : IGridObject
    {
        void Update(GameTime gameTime, float fps, Grid grid);
    }
}