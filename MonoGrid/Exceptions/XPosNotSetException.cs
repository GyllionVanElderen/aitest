﻿using System;

namespace MonoGrid.Exceptions
{
    public class XPosNotSetException : Exception
    {
        public XPosNotSetException()
        { 
        }

        public XPosNotSetException(string message)
            : base(message)
        {
        }

        public XPosNotSetException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}