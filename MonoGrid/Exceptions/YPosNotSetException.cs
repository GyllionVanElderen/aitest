﻿using System;

namespace MonoGrid.Exceptions
{
    public class YPosNotSetException : Exception
    {
        public YPosNotSetException()
        {
        }

        public YPosNotSetException(string message)
            : base(message)
        {
        }

        public YPosNotSetException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}