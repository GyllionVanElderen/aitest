﻿using System.Collections.Generic;
using MonoGrid.Enums;

namespace MonoGrid.Dictionaries
{
    public static class OppositeDirectionDictionary
    {
        public static Dictionary<Direction, Direction> OppositeDirections = new Dictionary<Direction, Direction>
        {
            { Direction.North, Direction.South},
            { Direction.South, Direction.North},
            { Direction.West, Direction.East},
            { Direction.East, Direction.West},
            { Direction.NorthWest, Direction.SouthEast},
            { Direction.NorthEast, Direction.SouthWest},
            { Direction.SouthWest, Direction.NorthEast},
            { Direction.SouthEast, Direction.NorthWest}
        };
    }
}