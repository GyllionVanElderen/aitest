﻿using Microsoft.Xna.Framework;

namespace MonoGrid.Helpers
{
    public static class RenderHelper
    {
        /// <summary>
        /// Calculation of where to render the grid object (in pixels)
        /// </summary>
        /// <param name="gridPosition">Which position in the grid? (x, y)</param>
        /// <param name="gridOffset">The offset of the grid itself (How far to the right? How far down?)</param>
        /// <param name="columnWidthHeight">The width and height of each column (They must be the same)</param>
        /// <returns>A Vector2 object with the right position to render the grid object on</returns>
        public static  Vector2 GetRenderPosition(Vector2 gridPosition, Vector2 gridOffset, float columnWidthHeight)
        {
            Vector2 renderPosition = new Vector2
            {
                X = (gridPosition.X * columnWidthHeight) + gridOffset.X,
                Y = (gridPosition.Y * columnWidthHeight) + gridOffset.Y
            };
            return renderPosition;
        }

        /// <summary>
        /// Calculate where in the grid square the texture needs to be rendered when it needs to be centered
        /// </summary>
        /// <param name="textureWidthHeight">The width and height of the texture</param>
        /// <param name="renderPosition">The original to render position</param>
        /// <param name="columnWidthHeight">The width and height of each column (They must be the same)</param>
        /// <returns>A Vector2 object with the right position to render the grid object on (GridObject will be centered)</returns>
        public static Vector2 GetRenderPositionCentered(Vector2 textureWidthHeight, Vector2 renderPosition, float columnWidthHeight)
        {
            Vector2 centeredRenderPosition = new Vector2
            {
                X = renderPosition.X + ((columnWidthHeight - textureWidthHeight.X) / 2),
                Y = renderPosition.Y + ((columnWidthHeight - textureWidthHeight.Y) / 2)
            };
            return centeredRenderPosition;
        }

        /// <summary>
        /// Calculation of where to render the grid object (in pixels)
        /// Compatible with MoveableGridObject (animationPosition added)
        /// </summary>
        /// <param name="gridPosition">Which position in the grid? (x, y)</param>
        /// <param name="animationPosition">Which position in the animation within the gridsquare?</param>
        /// <param name="gridOffset">The offset of the grid itself (How far to the right? How far down?)</param>
        /// <param name="columnWidthHeight">The width and height of each column (They must be the same)</param>
        /// <returns>A Vector2 object with the right position to render the grid object on</returns>
        public static Vector2 GetMoveableRenderPosition(Vector2 gridPosition, Vector2 animationPosition, Vector2 gridOffset, float columnWidthHeight)
        {
            Vector2 renderPosition = new Vector2
            {
                X = (gridPosition.X * columnWidthHeight) + gridOffset.X + animationPosition.X,
                Y = (gridPosition.Y * columnWidthHeight) + gridOffset.Y + animationPosition.Y
            };
            return renderPosition;
        }
    }
}