﻿namespace MonoGrid.Helpers
{
    public class FpsHelper
    {
        private int _cycle;

        public float GetFps(double seconds)
        {
            _cycle++;
            return _cycle / (float)seconds;
        }
    }
}