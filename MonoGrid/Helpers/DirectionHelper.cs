﻿using Microsoft.Xna.Framework;
using MonoGrid.Dictionaries;
using MonoGrid.Enums;

namespace MonoGrid.Helpers
{
    public static class DirectionHelper
    {
        /// <summary>
        /// Calculate the render position of a MoveableGridObject
        /// </summary>
        /// <param name="gridPositionCurrent">The current grid position</param>
        /// <param name="gridPositionTarget">The grid position to move to</param>
        /// <param name="columnWidthHeight">The width and height of every column in the grid</param>
        /// <param name="animationPosition">The current position within the animation. 0 = not moved at all, columnWidthHeight = moved</param>
        /// <returns>The calculated position to render the object in MonoGame</returns>
        public static Vector2 GetRenderPosition(Vector2 gridPositionCurrent, 
                                                Vector2 gridPositionTarget, 
                                                float columnWidthHeight,
                                                Vector2 gridOffset,
                                                float animationPosition)
        {
            Vector2 renderPosition = new Vector2((gridPositionCurrent.X * columnWidthHeight) + gridOffset.X, 
                                                 (gridPositionCurrent.Y * columnWidthHeight) + gridOffset.Y);

            // X movement
            if (gridPositionCurrent.X != gridPositionTarget.X)
            {
                renderPosition.X = ((columnWidthHeight * gridPositionCurrent.X) + (animationPosition * (gridPositionTarget.X - gridPositionCurrent.X))) + gridOffset.X;
            }

            // Y movement
            if (gridPositionCurrent.Y != gridPositionTarget.Y)
            {
                renderPosition.Y = ((columnWidthHeight * gridPositionCurrent.Y) + (animationPosition * (gridPositionTarget.Y - gridPositionCurrent.Y))) + gridOffset.Y;
            }

            return renderPosition;
        }

        /// <summary>
        /// Determines what the target position will be based on the current grid position and the direction to go
        /// </summary>
        /// <param name="gridPositionCurrent">The current grid position</param>
        /// <param name="direction">Which direction to go to</param>
        /// <returns>The target grid position</returns>
        public static Vector2 GetTargetPosition(Vector2 gridPositionCurrent, Direction direction)
        {
            Vector2 addition;

            switch (direction)
            {
                case Direction.West:
                    addition = new Vector2(-1, 0);
                    break;
                case Direction.East:
                    addition = new Vector2(1, 0);
                    break;
                case Direction.North:
                    addition = new Vector2(0, -1);
                    break;
                case Direction.South:
                    addition = new Vector2(0, 1);
                    break;
                case Direction.NorthWest:
                    addition = new Vector2(-1, -1);
                    break;
                case Direction.NorthEast:
                    addition = new Vector2(1, -1);
                    break;
                case Direction.SouthEast:
                    addition = new Vector2(1, 1);
                    break;
                case Direction.SouthWest:
                    addition = new Vector2(-1, 1);
                    break;
                default:
                    addition = new Vector2(0, 0);
                    break;
            }

            return Vector2.Add(gridPositionCurrent, addition);
        }

        /// <summary>
        /// Calculate how much pixels per loop to increase in order to make an object move
        /// </summary>
        /// <param name="distance">The distance to travel in pixels</param>
        /// <param name="timeInSeconds">The amount of time in seconds the travel needs to cost</param>
        /// <param name="loopsPerSecond">How much loops per second can be expected (FPS) of the Update() method</param>
        /// <returns></returns>
        public static float ToPixelsPerLoop(float distance, float timeInSeconds, float loopsPerSecond)
        {
            if (loopsPerSecond > 0)
            {
                float pixelsPerLoop = distance * (timeInSeconds / loopsPerSecond);
                return pixelsPerLoop;
            }
            return 0;
        }

        /// <summary>
        /// Retrieve the distance between a grid position and the end of the grid regarding a certain direction
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="GridPosition"></param>
        /// <returns></returns>
        public static float GetLeftOverDistance(Direction direction, int width, int height, Vector2 gridPosition)
        {
            switch (direction)
            {
                case Direction.North:
                    return gridPosition.Y;

                case Direction.South:
                    return (height - gridPosition.Y) - 1;

                case Direction.West:
                    return gridPosition.X;

                case Direction.East:
                    return (width - gridPosition.X) - 1;

                case Direction.NorthWest:
                    if (gridPosition.X > gridPosition.Y)
                    {
                        return gridPosition.Y;
                    }
                    else
                    {
                        return gridPosition.X;
                    }

                case Direction.NorthEast:
                    if ((gridPosition.X + gridPosition.Y) < width)
                    {
                        return gridPosition.Y;
                    }
                    else
                    {
                        return (width - gridPosition.X) - 1;
                    }

                case Direction.SouthWest:
                    if ((gridPosition.X + gridPosition.Y) < height)
                    {
                        return gridPosition.X;
                    }
                    else
                    {
                        return (height - gridPosition.Y) - 1;
                    }

                case Direction.SouthEast:
                    if (gridPosition.X < ((width - height) + gridPosition.Y))
                    {
                        return (height - gridPosition.Y) - 1;
                    }
                    else
                    {
                        return (width - gridPosition.X) - 1;
                    }
            }

            // No direction found
            return 0f;
        }

        public static Direction GetOppositeDirection(Direction direction)
        {
            return OppositeDirectionDictionary.OppositeDirections[direction];
        }
    }
}