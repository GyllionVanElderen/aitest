﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Helpers;

namespace MonoGrid.Field
{
    public class GridSquare
    {
        public Texture2D Texture { get; set; }
        public Point GridPosition { get; set; }

        private Point _renderPosition;
        private int _columnWidthHeight;

        public GridSquare(Texture2D texture, Vector2 gridPosition, Vector2 gridOffset, float columnWidthHeight)
        {
            Texture = texture;

            GridPosition = new Point((int)gridPosition.X, (int)gridPosition.Y);

            Vector2 renderPosition = RenderHelper.GetRenderPosition(gridPosition, gridOffset, columnWidthHeight);
            _renderPosition = new Point((int)renderPosition.X, (int)renderPosition.Y);
            _columnWidthHeight = (int)columnWidthHeight;
        }

        public void Draw(SpriteBatch spriteBatch, Color color)
        {
            // Draw upper line
            spriteBatch.Draw(Texture, new Rectangle(_renderPosition, new Point(_columnWidthHeight, 1)), color);

            // Draw lower line
            spriteBatch.Draw(Texture, new Rectangle(new Point(_renderPosition.X, _renderPosition.Y + _columnWidthHeight), new Point(_columnWidthHeight, 1)), color);

            // Draw left line
            spriteBatch.Draw(Texture, new Rectangle(new Point(_renderPosition.X, _renderPosition.Y), new Point(1, _columnWidthHeight)), color);

            // Draw right line
            spriteBatch.Draw(Texture, new Rectangle(new Point(_renderPosition.X + _columnWidthHeight, _renderPosition.Y), new Point(1, _columnWidthHeight)), color);
        }
    }
}