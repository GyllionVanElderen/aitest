﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGrid.Objects;

namespace MonoGrid.Field
{
    public class Grid
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public float ColumnWidthHeight { get; }

        public Vector2 Offset { get; }

        private readonly SpriteFont _spriteFont;
        private readonly List<GridSquare> _gridSquares;

        private readonly List<IGridObject> _gridObjects;

        public Grid(GraphicsDevice gdevice, SpriteFont spriteFont, int width, int height, float columnWidthHeight, Vector2 offset)
        {
            Width = width;
            Height = height;
            ColumnWidthHeight = columnWidthHeight;
            Offset = offset;

            // TODO Delete this after debugging
            _spriteFont = spriteFont;

            _gridObjects = new List<IGridObject>();

            // Set GridSquares
            Texture2D gridPixel = new Texture2D(gdevice, 1, 1);
            gridPixel.SetData(new []{Color.White});
            _gridSquares = new List<GridSquare>();
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    _gridSquares.Add(new GridSquare(gridPixel, new Vector2(j, i), Offset, ColumnWidthHeight));
                }
            }
        }

        public void Clear()
        {
            _gridObjects.Clear();
        }

        public void Add(IGridObject gridObject)
        {
            _gridObjects.Add(gridObject);
        }

        public void Update(GameTime gameTime, float fps)
        {
            foreach (IGridObject gridObject in _gridObjects)
            {
                if (gridObject is Objects.IUpdateable updateable)
                {
                    updateable.Update(gameTime, fps, this);
                }
            }
        }

        public void Draw(GameTime gameTime, float fps, SpriteBatch spriteBatch)
        {
            Vector2 cGridPosition = new Vector2();

            foreach (IGridObject gridObject in _gridObjects)
            {
                if (gridObject is Objects.IDrawable drawable)
                {
                    drawable.Draw(gameTime, fps, spriteBatch, Offset, ColumnWidthHeight, _spriteFont);
                }

                // TODO debug purposes
                if (gridObject is MoveableGridObject c)
                {
                    cGridPosition = c.GridPosition;
                }
            }

            foreach (GridSquare gridSquare in _gridSquares)
            {
                // TODO debug purposes
                if (IsVisionSquare(gridSquare, cGridPosition))
                {
                    gridSquare.Draw(spriteBatch, Color.Black);
                }
                else
                {
                    gridSquare.Draw(spriteBatch, Color.LightGray);
                }
            }
        }

        private bool IsVisionSquare(GridSquare gridSquare, Vector2 cGridPosition)
        {
            return gridSquare.GridPosition.X == cGridPosition.X - 1 && gridSquare.GridPosition.Y == cGridPosition.Y ||
                   gridSquare.GridPosition.X == cGridPosition.X + 1 && gridSquare.GridPosition.Y == cGridPosition.Y ||
                   gridSquare.GridPosition.X == cGridPosition.X && gridSquare.GridPosition.Y == cGridPosition.Y - 1 ||
                   gridSquare.GridPosition.X == cGridPosition.X && gridSquare.GridPosition.Y == cGridPosition.Y + 1 ||
                   gridSquare.GridPosition.X == cGridPosition.X - 1 && gridSquare.GridPosition.Y == cGridPosition.Y + 1 ||
                   gridSquare.GridPosition.X == cGridPosition.X - 1 && gridSquare.GridPosition.Y == cGridPosition.Y - 1 ||
                   gridSquare.GridPosition.X == cGridPosition.X + 1 && gridSquare.GridPosition.Y == cGridPosition.Y - 1 ||
                   gridSquare.GridPosition.X == cGridPosition.X + 1 && gridSquare.GridPosition.Y == cGridPosition.Y + 1;
        }

        /// <summary>
        /// Returns the gridobject on the specified grid position
        /// </summary>
        /// <param name="gridPosition">The grid position to search on</param>
        /// <returns>The grid object (if there is any)</returns>
        public IGridObject GetGridObjectObject(Vector2 gridPosition)
        {
            return _gridObjects.FirstOrDefault(x => Vector2.DistanceSquared(gridPosition, x.GridPosition) == 0);
        }
    }
}